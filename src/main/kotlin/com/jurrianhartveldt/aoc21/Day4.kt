package com.jurrianhartveldt.aoc21

import com.jurrianhartveldt.aoc21.Util.getFile
import java.io.File

class Day4(val filename: String) {

    private fun play(): MutableList<Board> {
        val lines = File(getFile(filename)).readLines()

        val input = lines[0].split(",").map { it.toInt() }

        val boards = lines.drop(1).withIndex().fold(mutableListOf<Board>()) { boardList, line ->
            if (line.index % 6 == 0) boardList.add(Board())
            boardList[line.index / 6].addLine(line.value)
            boardList
        }

        input.takeWhile { roundValue -> boards.map { it.newRound(roundValue) }.any { !it.hasWon() } }

        return boards
    }

    fun part1(): Int {
        return play().reduce { a: Board, b: Board -> if (a.roundsToWin() < b.roundsToWin()) a else b }.result()
    }

    fun part2(): Int {
        return play().reduce { a: Board, b: Board -> if (a.roundsToWin() > b.roundsToWin()) a else b }.result()
    }
}

class Board(
    private var calledRounds: MutableList<Int> = mutableListOf(),
    private var grid: MutableList<Int> = mutableListOf(),
    private var scores: MutableList<Int> = MutableList(10) { 0 },
    private var winningScoreIndex: Int = -1
) {

    fun addLine(line: String) {
        if (line.isBlank()) return

        grid.addAll(line.split(" ").filter { it.isNotBlank() }.map { it.toInt() })
    }

    fun newRound(value: Int): Board {
        if (hasWon()) {
            return this
        }

        calledRounds.add(value)
        val index = grid.indexOf(value)

        if (index == -1) {
            return this
        }

        scores[index % 5]++
        scores[5 + index / 5]++

        winningScoreIndex = scores.withIndex().find { it.value == 5 }?.index ?: -1

        return this
    }

    fun hasWon(): Boolean = winningScoreIndex != -1

    fun result(): Int = calledRounds.last() * uncalledEntries().sum()

    private fun uncalledEntries(): List<Int> = grid - calledRounds.toSet()

    fun roundsToWin(): Int = calledRounds.size
}

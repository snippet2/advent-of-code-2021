package com.jurrianhartveldt.aoc21

import com.jurrianhartveldt.aoc21.Util.getFile
import java.io.File

class Day2(val filename: String) {

    fun part1(): Int {
        var horizontal = 0
        var vertical = 0

        File(getFile(filename)).forEachLine {

            val line = it.split(" ")

            when (line[0]) {
                "forward" -> horizontal += line[1].toInt()
                "up" -> vertical -= line[1].toInt()
                "down" -> vertical += line[1].toInt()
            }
        }

        println("horizontal $horizontal x vertical $vertical")

        return horizontal * vertical
    }

    fun part2(): Int {
        var horizontal = 0
        var vertical = 0
        var aim = 0

        File(getFile(filename)).forEachLine {

            val line = it.split(" ")
            val amount = line[1].toInt()

            when (line[0]) {
                "down" -> aim += amount
                "up" -> aim -= amount
                "forward" -> {
                    horizontal += amount
                    vertical += (aim * amount)
                }


            }
        }

        println("horizontal $horizontal x vertical $vertical")

        return horizontal * vertical
    }
}

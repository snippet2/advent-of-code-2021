package com.jurrianhartveldt.aoc21

import com.jurrianhartveldt.aoc21.Util.getFile
import java.io.File

class Day6(val filename: String) {

    fun part(days: Int): Long {

        val fishes = MutableList<Long>(7, {0})
        File(getFile(filename)).readLines()[0].split(",").forEach { fishes[it.toInt()]++ }

        return countFishes(days, fishes)
    }

    private fun countFishes(days: Int, fishes: MutableList<Long>): Long {
        val newFishes = MutableList<Long>(9, {0})

        for (day in 1..days) {
            val currentIndex = (day-1) % 7

            fishes[currentIndex] += newFishes[0]
            (0..7).forEach { newFishes[it] = newFishes[it + 1] }
            newFishes[8] = fishes[currentIndex]
        }

        return fishes.sum() + newFishes.sum()
    }
}

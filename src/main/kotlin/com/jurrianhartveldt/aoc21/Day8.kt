package com.jurrianhartveldt.aoc21

import com.jurrianhartveldt.aoc21.Day8Util.existsInAll
import com.jurrianhartveldt.aoc21.Day8Util.minus
import com.jurrianhartveldt.aoc21.Day8Util.sort
import com.jurrianhartveldt.aoc21.Util.getFile
import java.io.File

class Day8(val filename: String) {

    fun part1(): Int =
        File(getFile(filename)).readLines().fold(listOf<SevenSegmentDisplay>()) { list, line ->
            val inputOutput = line.split(" | ")
            list + SevenSegmentDisplay(inputOutput[0].split(" "), inputOutput[1].split(" "))
        }.sumOf { it.count1478OutputOccurence() }

    fun part2(): Int =
        File(getFile(filename)).readLines().fold(listOf<SevenSegmentDisplay>()) { list, line ->
            val inputOutput = line.split(" | ")
            list + SevenSegmentDisplay(
                inputOutput[0].split(" ").map { it.sort() },
                inputOutput[1].split(" ").map { it.sort() })
        }.sumOf { it.decodeOutput() }
}

class SevenSegmentDisplay(val signalPatterns: List<String>, val outputValues: List<String>) {

    fun count1478OutputOccurence() = outputValues.filter {
        when (it.length) {
            2, 3, 4, 7 -> true
            else -> false
        }
    }.count()

    fun decodeOutput(): Int {
        val mapping = deduceMapping()

        return outputValues.map { mapping[it] }.joinToString("").toInt()
    }

    private fun deduceMapping(): Map<String, Int> {

        val numbers = signalPatterns
            .filter {
                when (it.length) {
                    2, 3, 4, 7 -> true
                    else -> false
                }
            }
            .fold(mapOf<Int, String>()) { map, input ->
                map + when (input.length) {
                    2 -> 1 to input
                    3 -> 7 to input
                    4 -> 4 to input
                    else -> 8 to input // ( 8 --> input to 8)
                }
            }.toMutableMap()

        val size6Words = signalPatterns.filter { it.length == 6 }
        val size5Words = signalPatterns.filter { it.length == 5 }
        val charByOccurance = signalPatterns.joinToString("")
            .toCharArray().groupBy { it }.map { it.key to it.value.size }.toMap()

        val a = numbers[7]!!.minus(numbers[1]!!) // unused
        val f = numbers[1]!!.filter { it.existsInAll(size6Words) }
        val c = numbers[1]!!.minus(f)
        val e: String = charByOccurance.entries.filter { it.value == 4 }.first().key.toString()

        numbers += (3 to size5Words.filter { word ->
            numbers[7]!!.toCharArray().all { word.contains(it) }
        }[0])

        numbers += (5 to size5Words.filter { !it.contains(c) }[0])
        numbers += (2 to size5Words.filter { !listOf(numbers[3], numbers[5]).contains(it) }[0])

        numbers += (9 to numbers[8]!!.minus(e))
        numbers += (6 to size6Words.filter { !it.contains(c) }[0])
        numbers += (0 to size6Words.filter { !listOf(numbers[6], numbers[9]).contains(it) }[0])

        return numbers.entries.fold(mapOf()) { map, pair -> map + (pair.value.sort() to pair.key) }
    }

}

object Day8Util {
    fun String.minus(word: String) = this.filter { !word.contains(it) }

    fun String.sort() = this.toCharArray().apply { sort() }.concatToString()

    fun Char.existsInAll(words: List<String>) = words.all { it.contains(this) }
}

package com.jurrianhartveldt.aoc21

import com.jurrianhartveldt.aoc21.Util.getFile
import java.io.File
import java.lang.Math.abs

class Day7(val filename: String) {

    fun part1(): Long = calcMinimalFuel(calcFuel1)

    fun part2(): Long = calcMinimalFuel(calcFuel2)

    private fun calcMinimalFuel(fuelMethod: (Map<Int, Int>, Int) -> Long): Long {
        val crabs = mutableMapOf<Int, Int>()

        File(getFile(filename)).readLines()[0].split(",")
            .forEach { crabs[it.toInt()] = crabs.getOrDefault(it.toInt(), 0) + 1 }

        val min = crabs.keys.fold(Int.MAX_VALUE) { min, current -> kotlin.math.min(min, current) }
        val max = crabs.keys.fold(Int.MIN_VALUE) { max, current -> kotlin.math.max(max, current) }

        // brute-force this
        return (min..max).fold(Long.MAX_VALUE) { minimal, index ->
            kotlin.math.min(minimal, fuelMethod(crabs, index))
        }
    }

    private val calcFuel1 = fun(crabs: Map<Int, Int>, index: Int) =
        crabs.entries.fold(0L) { fuelCost, crab -> fuelCost + abs(index - crab.key) * crab.value }

    private val calcFuel2 = fun(crabs: Map<Int, Int>, index: Int) =
        crabs.entries
            .fold(0L) { fuelCost, crab ->
                fuelCost +
                        if (index == crab.key) 0
                        else (1..abs(index - crab.key)).fold(0) { acc, i -> acc + i } * crab.value
            }
}

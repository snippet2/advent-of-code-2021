package com.jurrianhartveldt.aoc21

import com.jurrianhartveldt.aoc21.Util.getFile
import java.io.File
import java.lang.Character.getNumericValue
import kotlin.math.pow

class Day3(val filename: String, val lineSize: Int) {

    fun part1(): Int {

        val sum = MutableList(lineSize) { 0 }

        File(getFile(filename)).forEachLine {

            it.iterator().withIndex().forEach {
                if (it.value == '0') sum[it.index]-- else sum[it.index]++
            }

        }

        val gammaByte = sum.map { toBit(it) }.joinToString(separator = "")

        val gamma = toByte(gammaByte)
        val epsilon = toByte(invert(gammaByte))

        println("gamma: $gamma * epsilon: $epsilon")

        return gamma * epsilon
    }

    fun part2(): Int {
        val lines = File(getFile(filename)).readLines().map { it to it }

        val oxygenGeneratorRatingListResult = getRatingList(FilterType.MAJORITY, lines)
        val co2ScrubberRatingListResult = getRatingList(FilterType.MINORITY, lines)

        println("oxygenGeneratorRatingList $oxygenGeneratorRatingListResult")
        println("co2ScrubberRatingList $co2ScrubberRatingListResult")

        val oxygenGeneratorRating = toByte(oxygenGeneratorRatingListResult[0].second)
        val co2ScrubberRating = toByte(co2ScrubberRatingListResult[0].second)

        return oxygenGeneratorRating * co2ScrubberRating
    }

    enum class FilterType {
        MAJORITY, MINORITY
    }

    fun getRatingList(filterType: FilterType, inputLines: List<Pair<String, String>>): List<Pair<String, String>> {
        var lines = inputLines.toList()
        do {
            lines = filterLeading(filterType, lines).map { (first, second) -> first.substring(1) to second }
        } while (lines.size > 1)

        return lines
    }

    fun filterLeading(filterType: FilterType, lines: List<Pair<String, String>>): List<Pair<String, String>> {
        val groups = lines.groupBy { getNumericValue(it.first[0]) }

        if (groups[0]?.size == groups[1]?.size) {
            return if (filterType == FilterType.MAJORITY) groups[1]!! else groups[0]!!
        }

        val largest = if (groups[0]!!.size > groups[1]!!.size) groups[0]!! else groups[1]!!
        val smallest = if (groups[0]!!.size < groups[1]!!.size) groups[0]!! else groups[1]!!

        return if (filterType == FilterType.MAJORITY) largest else smallest
    }

    fun toBit(input: Int) = if (input > 0) 1 else 0

    fun toByte(input: String): Int =
        input.reversed().withIndex().sumOf { (getNumericValue(it.value) * 2.0.pow(it.index.toDouble())).toInt() }

    fun invert(input: String): String = input.map { if (it == '0') '1' else '0' }.joinToString(separator = "")
}

package com.jurrianhartveldt.aoc21

import com.jurrianhartveldt.aoc21.Util.getFile
import java.io.File

class Day1(val filename: String) {

    fun part1(): Int {
        var count = 0
        var previousNumber = Int.MAX_VALUE

        File(getFile(filename)).forEachLine {

            val number = it.trim().toInt()
            if (number > previousNumber) count++

            previousNumber = number
        }

        return count
    }

    fun part2(): Int {
        var count = 0
        val windowGroups = mutableListOf(0, 0, 0, 0)
        var lineNr = 0;

        File(getFile(filename)).forEachLine {
            val line = it.split(" ")
            val number = line[0].toInt()

            val checkIndex = lineNr % 4
            val i0 = checkIndex % 4
            val i1 = (checkIndex + 1) % 4
            val i2 = (checkIndex + 2) % 4
            val i3 = (checkIndex + 3) % 4

//            println("state: $windowGroups" )

            val i1Total = windowGroups[i1]

            windowGroups[i0] += number
            windowGroups[i1] = 0
            windowGroups[i2] += number
            windowGroups[i3] += number

            if (lineNr >= 3) {
                if (i1Total < windowGroups[i2]) count++
//                println("$lineNr :: [$i1]${i1Total} < [$i2]${windowGroups[i2]} --> $count")
            }

            lineNr++
        }

        return count
    }
}

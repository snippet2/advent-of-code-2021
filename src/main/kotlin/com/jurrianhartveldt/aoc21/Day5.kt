package com.jurrianhartveldt.aoc21

import com.jurrianhartveldt.aoc21.Util.getFile
import java.io.File
import java.lang.Integer.max
import java.lang.Integer.min

operator fun Coord.rangeTo(other: Coord) = CoordProgression(this, other)

class Day5(val filename: String) {

    fun part1(): Int = scan(part1CoordsStrategy)

    fun part2(): Int = scan(part2CoordsStrategy)

    private fun scan(coordMethod: (List<Int>) -> List<String>): Int =
        File(getFile(filename)).readLines().fold(mutableMapOf<String, Int>().withDefault { 0 }) { map, line ->
            coordMethod(line.split(",", " -> ").map { it.toInt() })
                .forEach { map[it] = map.getValue(it) + 1 }
            map
        }.filter { it.value >= 2 }.count()

    private val part1CoordsStrategy = fun(vec: List<Int>): List<String> =
        when {
            vec[0] != vec[2] && vec[1] != vec[3] -> emptyList()
            vec[0] == vec[2] -> (min(vec[1], vec[3])..max(vec[1], vec[3]))
                .fold(listOf()) { list, y -> list + "${vec[0]}:$y" }
            else -> (min(vec[0], vec[2])..max(vec[0], vec[2]))
                .fold(listOf()) { list, x -> list + "$x:${vec[1]}" }
        }

    private val part2CoordsStrategy = fun(vec: List<Int>): List<String> =
        (Coord(vec[0], vec[1])..Coord(vec[2], vec[3])).fold(listOf()) { list, coord -> list + "${coord.x}:${coord.y}" }
}

data class Coord(val x: Int, val y: Int) : Comparable<Coord> {
    override fun compareTo(other: Coord): Int = if (this.x - other.x != 0) this.x - other.x else this.y - other.y
}

data class Vector(val x: Int, val y: Int)

class CoordIterator(
    startCoord: Coord,
    val endCoord: Coord,
    val stepVector: Vector
) : Iterator<Coord> {
    private var currentCoord = startCoord

    override fun hasNext(): Boolean {
        val hasNextX = if (stepVector.x == -1) currentCoord.x >= endCoord.x else currentCoord.x <= endCoord.x
        val hasNextY = if (stepVector.y == -1) currentCoord.y >= endCoord.y else currentCoord.y <= endCoord.y

        return hasNextX && hasNextY
    }

    override fun next(): Coord {
        val nextCoord = currentCoord
        currentCoord = Coord(currentCoord.x + stepVector.x, currentCoord.y + stepVector.y)
        return nextCoord
    }
}

class CoordProgression(
    override val start: Coord,
    override val endInclusive: Coord
) :
    Iterable<Coord>, ClosedRange<Coord> {

    override fun iterator(): Iterator<Coord> =
        CoordIterator(start, endInclusive, stepVector())

    fun stepVector(): Vector {
        var x = endInclusive.x - start.x
        var y = endInclusive.y - start.y

        x = if (x <= -1) -1 else if (x >= 1) 1 else 0
        y = if (y <= -1) -1 else if (y >= 1) 1 else 0

        return Vector(x, y)
    }

    infix fun step(days: Long) = CoordProgression(start, endInclusive)
}

package com.jurrianhartveldt.aoc21

import com.jurrianhartveldt.aoc21.Util.getFile
import java.io.File

class Day13(val filename: String) {

    fun part(foldLimit: Int): Int {
        var limit = foldLimit
        return File(getFile(filename)).readLines().fold(emptySet<Pair<Int, Int>>()) { set, line ->
            when {
                line.startsWith("fold along") -> {
                    if (limit > 0) {
                        limit--
                        line.foldAlongLine(set)
                    } else set
                }
                line.isBlank() -> set
                else -> set + (line.split(",")[0].toInt() to line.split(",")[1].toInt())
            }
        }.size
    }

    private fun String.foldAlongLine(set: Set<Pair<Int, Int>>): Set<Pair<Int, Int>> {
        val pairs = if (this.contains("x")) foldToLeft(this.split("=")[1].toInt(), set)
        else foldToTop(this.split("=")[1].toInt(), set)

        println(this)
        for (y in 0..pairs.maxOf { it.second }) {
            for (x in 0..pairs.maxOf { it.first }) {
                print(if (pairs.contains(x to y)) "#" else ".")
            }
            println()
        }

        return pairs
    }

    private fun foldToLeft(x: Int, set: Set<Pair<Int, Int>>): Set<Pair<Int, Int>> =
        set.map { if (it.first > x) (x - (it.first - x) to it.second) else it }.toSet()

    private fun foldToTop(y: Int, set: Set<Pair<Int, Int>>): Set<Pair<Int, Int>> =
        set.map { if (it.second > y) it.first to (y - (it.second - y)) else it }.toSet()
}

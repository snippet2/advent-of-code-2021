package com.jurrianhartveldt.aoc21

import com.jurrianhartveldt.aoc21.Util.getFile
import java.io.File

class Day9(val filename: String) {

    fun runPart1() = run(part1)

    fun runPart2() = run(part2)

    private fun run(part: (heightMap: List<Int>, width: Int) -> Int): Int {
        val lines = File(getFile(filename)).readLines()
        val width = lines[0].length

        val heightMap =
            lines.fold(listOf<Int>()) { list, line ->
                list + line.toCharArray().map { it.toString().toInt() }.toList()
            }

        println("width $width x height ${lines.size}")

        return part(heightMap, width)
    }

    private val part1 = fun(heightMap: List<Int>, width: Int): Int =
        heightMap.withIndex().fold(listOf<Int>()) { lowPoints, loc ->
            val x = loc.index % width
            val y = loc.index / width

            val north = if (y == 0) 9 else heightMap[loc.index - width]
            val west = if (x == 0) 9 else heightMap[loc.index - 1]
            val east = if (x == width - 1) 9 else heightMap[loc.index + 1]
            val south = if (y == (heightMap.size / width) - 1) 9 else heightMap[loc.index + width]

            if (loc.value < listOf(north, west, east, south).minOf { it }) {
                lowPoints + loc.value
            } else lowPoints
        }.map { it + 1 }.sum()

    private val part2 = fun(heightMap: List<Int>, width: Int): Int {
        val basinMap = MutableList(heightMap.size) { -1 }

        heightMap.asSequence().withIndex().map { it.index to it.value }
            .filter { it.second != 9 && basinMap[it.first] == -1 }
            .map { it.first }.withIndex()
            .forEach { traverseBasin(IndexedValue(it.value, it.index), heightMap, basinMap, width) }

        return basinMap.groupBy { it }
            .filter { it.key != -1 }
            .map { entry -> entry.value.size }
            .sortedByDescending { it }
            .take(3)
            .fold(1) { result, basinSize -> result * basinSize }
    }

    private fun traverseBasin(
        basinLoc: IndexedValue<Int>,
        heightMap: List<Int>,
        basinMap: MutableList<Int>,
        width: Int
    ) {
        if (basinMap[basinLoc.index] >= 0 || heightMap[basinLoc.index] == 9) {
            return
        }

        basinMap[basinLoc.index] = basinLoc.value

        val x = basinLoc.index % width
        val y = basinLoc.index / width

        // north
        if (y != 0) {
            val north = IndexedValue(basinLoc.index - width, basinLoc.value)
            traverseBasin(north, heightMap, basinMap, width)
        }

        // west
        if (x != 0) {
            val west = IndexedValue(basinLoc.index - 1, basinLoc.value)
            traverseBasin(west, heightMap, basinMap, width)
        }

        // east
        if (x != width - 1) {
            val east = IndexedValue(basinLoc.index + 1, basinLoc.value)
            traverseBasin(east, heightMap, basinMap, width)
        }

        // south
        if (y != heightMap.size / width - 1) {
            val south = IndexedValue(basinLoc.index + width, basinLoc.value)
            traverseBasin(south, heightMap, basinMap, width)
        }
    }
}

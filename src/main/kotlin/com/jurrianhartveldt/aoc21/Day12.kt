package com.jurrianhartveldt.aoc21

import com.jurrianhartveldt.aoc21.Util.getFile
import java.io.File

class Day12(val filename: String) {

    fun part(roomForOneMoreSmallStop: Boolean): Int {
        val nodes = parseNodes()

        return nodes
            .filter { it.value.type == NodeType.START }.map { it.value }.first()
            .let { chartRoutes("start,", it, nodes, roomForOneMoreSmallStop) }.size
    }

    private fun parseNodes() = File(getFile(filename)).readLines()
        .fold(emptyMap<String, Node>()) { map, line -> map + map.addNewRoute(line.split("-")) }
}

private fun chartRoutes(
    trail: String,
    current: Node,
    nodes: Map<String, Node>,
    roomForOneMoreSmall: Boolean
): Set<String> =
    current.routesTo
        .asSequence()
        .map { nodes[it]!! }
        .filterNot { it.type == NodeType.START }
        .fold(emptySet()) { set, node ->
            set + when {
                node.type == NodeType.END -> setOf(trail.dropLast(1))
                node.type == NodeType.SMALL && trail.contains(",${node.name},") ->
                    if (roomForOneMoreSmall) chartRoutes("$trail${node.name},", node, nodes, false)
                    else emptySet()
                else -> chartRoutes("$trail${node.name},", node, nodes, roomForOneMoreSmall)
            }
        }

private fun Map<String, Node>.addNewRoute(nodes: List<String>): Map<String, Node> {
    val left = this.getOrElse(nodes[0]) { Node(nodes[0], nodes[0].determineType()) }
    val right = this.getOrElse(nodes[1]) { Node(nodes[1], nodes[1].determineType()) }

    left.routesTo += right.name
    right.routesTo += left.name

    return mapOf(left.name to left, right.name to right)
}

private fun String.determineType(): NodeType =
    when (this) {
        "start" -> NodeType.START
        "end" -> NodeType.END
        else -> when {
            this.toCharArray()[0] in 'a'..'z' -> NodeType.SMALL
            else -> NodeType.LARGE
        }
    }

enum class NodeType {
    START,
    END,
    SMALL,
    LARGE
}

data class Node(val name: String, val type: NodeType, var routesTo: Set<String> = setOf())

package com.jurrianhartveldt.aoc21

object Util {
    fun getFile(filename: String): String {
        val file = javaClass.classLoader.getResource(filename)?.file

        println("evaluating: $file")

        return file!!
    }
}


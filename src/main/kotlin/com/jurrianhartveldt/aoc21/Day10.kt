package com.jurrianhartveldt.aoc21

import com.jurrianhartveldt.aoc21.Util.getFile
import java.io.File

class Day10(val filename: String) {

    private val chunks = listOf(
        Chunk('(', ')', 3, 1),
        Chunk('[', ']', 57, 2),
        Chunk('{', '}', 1197, 3),
        Chunk('<', '>', 25137, 4)
    )

    fun part1(): Long = File(getFile(filename)).readLines()
        .map { analyse(it) }
        .filter { it < 0 }
        .sumOf { kotlin.math.abs(it) }

    fun part2(): Long = File(getFile(filename)).readLines()
        .map { analyse(it) }
        .filter { it >= 0 }
        .sortedBy { it }
        .middleValue()

    private fun analyse(line: String): Long {
        val stack = ArrayDeque<Char>()

        line.toCharArray().forEach {
            when {
                it.isOpening() -> stack.add(it)
                stack.last().matchingBracket(it) -> stack.removeLast()
                it.isClosing() -> {
                    // Corrupted
                    return -1L * it.errorScore()
                }
            }
        }

        return stack.reversed().map { it.getClosingPoint() }
            .fold(0) { total, score -> if (score < 0) total else total * 5 + score }
    }

    private fun Char.isOpening(): Boolean = chunks.any { it.open == this }

    private fun Char.isClosing(): Boolean = chunks.any { it.close == this }

    private fun Char.errorScore(): Int = chunks.firstOrNull { it.close == this }?.errorPoints ?: 0

    private fun Char.matchingBracket(close: Char): Boolean = chunks.any { this == it.open && close == it.close }

    private fun Char.getClosingPoint(): Int = chunks.firstOrNull { it.open == this }?.closingPoints ?: 0

    private fun List<Long>.middleValue(): Long = this[this.size / 2]
}

data class Chunk(val open: Char, val close: Char, val errorPoints: Int, val closingPoints: Int)

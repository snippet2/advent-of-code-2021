package com.jurrianhartveldt.aoc21

import com.jurrianhartveldt.aoc21.Util.getFile
import java.io.File

class Day11(val filename: String) {

    fun part1(days: Int): Int {
        val dos = getDumboOctopi()

        for (day in 1..days) dos.simulateDay(day)

        return dos.sumOf { it.totalFlashes }
    }

    fun part2(): Int {
        val dos = getDumboOctopi()
        var step = 0

        do {
            dos.simulateDay(++step)
        } while (dos.notAllFlashInSync())

        return step
    }

    private fun getDumboOctopi(): List<DumboOctopus> =
        File(getFile(filename)).readLines().flatMap { line -> line.toCharArray().toList() }.withIndex()
            .fold(emptyList<DumboOctopus>()) { list, initChar ->
                list + DumboOctopus(initChar.value.toString().toInt(), initChar.index)
            }

    private fun List<DumboOctopus>.notAllFlashInSync(): Boolean = this.filter { it.energy == 0 }.count() != this.size

    private fun List<DumboOctopus>.simulateDay(day: Int) {
        // increase 1
        forEach {
            it.energy++
        }

        // keep flashin
        do {
            forEach { this.settleFlashes(it, day) }
        } while (this.any { it.incomingFlashes > 0 })
    }
}

private fun List<DumboOctopus>.settleFlashes(dumboOctopus: DumboOctopus, day: Int) {
    if (dumboOctopus.absorbIncoming(day)) {
        this.filter { it.isNeighbor(dumboOctopus) }.forEach { it.flash() }
    }
}

data class DumboOctopus(
    var energy: Int,
    val loc: Int,
    var incomingFlashes: Int = 0,
    var totalFlashes: Int = 0,
    var lastFlashedDay: Int = -1
) {

    private val neighborLocs = (0 until 9)
        .map { loc + ((it / 3) * 10 - 10) + (it % 3) - 1 }
        .filter { it != loc } // filter out myself
        .filter { it in 0..99 } // filter out y out of bounds
        .filterNot { loc % 10 == 0 && it % 10 == 9 } // filter out of west bound
        .filterNot { it % 10 == 0 && loc % 10 == 9 } // filter out of east bound

    fun flash() = incomingFlashes++

    fun absorbIncoming(day: Int): Boolean =
        if (energy + incomingFlashes > 9 && day > lastFlashedDay) {
            energy = 0
            incomingFlashes = 0
            lastFlashedDay = day
            totalFlashes++
            true // flash
        } else {
            energy = if (day == lastFlashedDay) 0 else energy + incomingFlashes
            incomingFlashes = 0
            false // absorbed without flash
        }

    fun isNeighbor(dumboOctopus: DumboOctopus): Boolean = neighborLocs.contains(dumboOctopus.loc)
}

package com.jurrianhartveldt.aoc21

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

internal class Day7Test() {

    @Test
    fun day7Example1() {
        Day7("day7/example.txt").part1() shouldBe 37
    }

    @Test
    fun day7Result() {
        val result = Day7("day7/input.txt").part1()

        println("result: $result")
    }

    @Test
    fun day7part2Example1() {
        Day7("day7/example.txt").part2() shouldBe 168
    }

    @Test
    fun day7part2Result() {
        val result = Day7("day7/input.txt").part2()

        println("result: $result")
    }
}

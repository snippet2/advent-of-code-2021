package com.jurrianhartveldt.aoc21

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

internal class Day3Test() {

    @Test
    fun day3Example() {
        Day3("day3/example1.txt", 5).part1() shouldBe 198
    }

    @Test
    fun day3Result() {
        val result = Day3("day3/input.txt", 12).part1()

        println("result: $result")
    }

    @Test
    fun day3Part2Example() {
        Day3("day3/example1.txt", 5).part2() shouldBe 230
    }


    @Test
    fun day3part2Result() {
        val result = Day3("day3/input.txt", 12).part2()

        println("result: $result")
    }
}

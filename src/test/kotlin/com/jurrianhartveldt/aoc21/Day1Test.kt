package com.jurrianhartveldt.aoc21

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

internal class Day1Test() {

    @Test
    fun day1Example() {
        Day1("day1/example1.txt").part1() shouldBe 7
    }

    @Test
    fun day1Result() {
        val result = Day1("day1/input.txt").part1()

        println("result: $result")
    }

    @Test
    fun day1part2Example() {
        Day1("day1/example2.txt").part2() shouldBe 5
    }

    @Test
    fun day1part2Result() {
        val result = Day1("day1/input.txt").part2()

        println("result: $result")
    }
}

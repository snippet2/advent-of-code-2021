package com.jurrianhartveldt.aoc21

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

internal class Day5Test() {

    @Test
    fun day5Example() {
        Day5("day5/example1.txt").part1() shouldBe 5
    }

    @Test
    fun day5part2Example() {
        Day5("day5/example1.txt").part2() shouldBe 12
    }

    @Test
    fun day5Result() {
        val result = Day5("day5/input.txt").part1()

        println("result: $result")
    }


    @Test
    fun day5part2Result() {
        val result = Day5("day5/input.txt").part2()

        println("result: $result")
    }
}

package com.jurrianhartveldt.aoc21

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

internal class Day2Test() {

    @Test
    fun day2Example() {
        Day2("day2/example1.txt").part1() shouldBe 150
    }

    @Test
    fun day2Result() {
        val result = Day2("day2/input.txt").part1()

        println("result: $result")
    }

    @Test
    fun day2part2Example() {
        Day2("day2/example1.txt").part2() shouldBe 900
    }

    @Test
    fun day2part2Result() {
        val result = Day2("day2/input.txt").part2()

        println("result: $result")
    }
}

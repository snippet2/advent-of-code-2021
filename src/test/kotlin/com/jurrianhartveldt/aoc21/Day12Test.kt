package com.jurrianhartveldt.aoc21

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

internal class Day12Test() {

    @Test
    fun day12Example1() {
        Day12("day12/example1.txt").part(false) shouldBe 10
    }

    @Test
    fun day12Example2() {
        Day12("day12/example2.txt").part(false) shouldBe 19
    }

    @Test
    fun day12Result() {
        val result = Day12("day12/input.txt").part(false)

        println("result: $result")
    }

    @Test
    fun day12part2Example1() {
        Day12("day12/example1.txt").part(true) shouldBe 36
    }

    @Test
    fun day12part2Example2() {
        Day12("day12/example2.txt").part(true) shouldBe 103
    }

    @Test
    fun day12part2Result() {
        val result = Day12("day12/input.txt").part(true)

        println("result: $result")
    }
}

package com.jurrianhartveldt.aoc21

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

internal class Day6Test() {

    @Test
    fun day6Example1() {
        Day6("day6/example1.txt").part(18) shouldBe 26
    }

    @Test
    fun day6Example2() {
        Day6("day6/example1.txt").part(80) shouldBe 5934
    }

    @Test
    fun day6Result() {
        val result = Day6("day6/input.txt").part(80)

        println("result: $result")
    }

    @Test
    fun day6part2Example2() {
        Day6("day6/example1.txt").part(256) shouldBe 26984457539
    }

    @Test
    fun day6part2Result() {
        val result = Day6("day6/input.txt").part(256)

        println("result: $result")
    }

}

package com.jurrianhartveldt.aoc21

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

internal class Day10Test() {

    @Test
    fun day10Example1() {
        Day10("day10/example.txt").part1() shouldBe 26397
    }

    @Test
    fun day10Result() {
        val result = Day10("day10/input.txt").part1()

        println("result: $result")
    }

    @Test
    fun day10part2Example1() {
        Day10("day10/example.txt").part2() shouldBe 288957
    }


    @Test
    fun day10part2Result() {
        val result = Day10("day10/input.txt").part2()

        println("result: $result")
    }
}

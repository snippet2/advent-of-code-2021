package com.jurrianhartveldt.aoc21

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

internal class Day4Test() {

    @Test
    fun day4Example() {
        Day4("day4/example1.txt").part1() shouldBe 4512
    }

    @Test
    fun day4part2Example() {
        Day4("day4/example1.txt").part2() shouldBe 1924
    }

    @Test
    fun day4Result() {
        val result = Day4("day4/input.txt").part1()

        println("result: $result")
    }

    @Test
    fun day4part2Result() {
        val result = Day4("day4/input.txt").part2()

        println("result: $result")
    }
}

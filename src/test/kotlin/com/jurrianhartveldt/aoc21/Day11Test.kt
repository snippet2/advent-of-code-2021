package com.jurrianhartveldt.aoc21

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

internal class Day11Test() {

    @Test
    fun day11Example1() {
        Day11("day11/example.txt").part1(10) shouldBe 204
    }

    @Test
    fun day11Result() {
        val result = Day11("day11/input.txt").part1(100)

        println("result: $result")
    }

    @Test
    fun day11part2Example1() {
        Day11("day11/example.txt").part2() shouldBe 195
    }

    @Test
    fun day11part2Result() {
        val result = Day11("day11/input.txt").part2()

        println("result: $result")
    }
}

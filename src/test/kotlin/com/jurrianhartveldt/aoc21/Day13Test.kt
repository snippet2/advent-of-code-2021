package com.jurrianhartveldt.aoc21

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

internal class Day13Test() {

    @Test
    fun day13Example1() {
        Day13("day13/example1.txt").part(1) shouldBe 17
    }

    @Test
    fun day13Example2() {
        Day13("day13/example1.txt").part(100) shouldBe 16
    }

    @Test
    fun day13Result() {
        val result = Day13("day13/input.txt").part(1)

        println("result: $result")
    }

    @Test
    fun day13part2Result() {
        val result = Day13("day13/input.txt").part(100)

        println("result: $result")
    }
}

package com.jurrianhartveldt.aoc21

import com.jurrianhartveldt.aoc21.Day8Util.existsInAll
import com.jurrianhartveldt.aoc21.Day8Util.minus
import com.jurrianhartveldt.aoc21.Day8Util.sort
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

internal class Day8Test() {

    @Test
    fun day8Example1() {
        Day8("day8/example1.txt").part1() shouldBe 0
    }

    @Test
    fun day8Example2() {
        Day8("day8/example2.txt").part1() shouldBe 26
    }

    @Test
    fun day8Result() {
        val result = Day8("day8/input.txt").part1()

        println("result: $result")
    }

    @Test
    fun day8part2Example1() {
        Day8("day8/example1.txt").part2() shouldBe 5353
    }

    @Test
    fun day8part2Example2() {
        Day8("day8/example2.txt").part2() shouldBe 61229
    }

    @Test
    fun day8part2Example3() {
        Day8("day8/example3.txt").part2() shouldBe 8394
    }

    @Test
    fun day8part2Result() {
        val result = Day8("day8/input.txt").part2()

        println("result: $result")
    }

    @Test
    fun testMinus() {
        "abc".minus("ac") shouldBe "b"
    }

    @Test
    fun testExistsInAll() {
        'f'.existsInAll(listOf("abdefg", "abcdfg", "abcefg")) shouldBe true
        'c'.existsInAll(listOf("abdefg", "abcdfg", "abcefg")) shouldBe false
        "cf".filter { it.existsInAll(listOf("abdefg", "abcdfg", "abcefg")) } shouldBe "f"
    }

    @Test
    fun testSort() {
        "zyxcdab".sort() shouldBe "abcdxyz"
    }
}

package com.jurrianhartveldt.aoc21

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

internal class Day9Test() {

    @Test
    fun day9Example1() {
        Day9("day9/example.txt").runPart1() shouldBe 15
    }

    @Test
    fun day9Result() {
        val result = Day9("day9/input.txt").runPart1()

        println("result: $result")
    }

    @Test
    fun day9part2Example1() {
        Day9("day9/example.txt").runPart2() shouldBe 1134
    }

    @Test
    fun day9part2Result() {
        val result = Day9("day9/input.txt").runPart2()

        println("result: $result")
    }
}
